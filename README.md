# credit (crypted edit)

A simple system for editing text files encrypted with GPG using whatever private GPG you have marked as your default.
This decrypts your file to ``/dev/shm`` (tmpfs), opens the ``$EDITOR`` text editor, and then saves the file back to the original file.
In theory, no artefacts from your decrypted files remain (ignoring various questions of RAM halflife, swapping, and so on).

This is largely just the ``cmd_edit`` function from https://www.passwordstore.org/, with a few adjustments.

## Usage

You must have GPG setup on your machine, and you must have declared a default key.
If you're unsure whether you have a default key, run this command:

```
$ gpgconf --list-options gpg | grep default-key 
```

Assuming you have that set up, use this application in a terminal.
To edit a file called ``foo.gpg``, for example:

```
$ credit foo.gpg
```

If ``foo.gpg`` doesn't exist, a new file is created.

