Name:           credit
Version:        0.0.3
Release:        1%{?dist}
Summary:        Edit encrypted files in tmpfs

License:        GPLv3+
URL:            https://www.gitlab.com/notklaatu/%{name}
Source0:        https://gitlab.com/notklaatu/%{name}/-/archive/master/%{name}-master.tar.bz2

BuildArch:      noarch

%description
A simple system for editing text files encrypted with GPG using whatever private GPG you have marked as your default. This decrypts your file to /dev/shm (tmpfs), opens a text editor, and then saves the file back to the original file. In theory, no artefacts from your decrypted files remain (ignoring various questions of RAM halflife, swapping, and so on).

%prep
%setup -q -n %{name}-master

%build
%install

mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_mandir}
mkdir -p %{buildroot}/%{_infodir}

install -m 0755 %{name} %{buildroot}/%{_bindir}/%{name}

%files
%license LICENSE
%{_bindir}/%{name}
%doc README.md 

%changelog
* Wed Feb 5 2020 Klaatu <klaatu@fedorapeople.org> - 0.0.3
- Initial RPM spec file

